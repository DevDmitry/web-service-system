package com.serviceSystem.DAO.builder;

public class ColumnNames {
    public static final String ORDER_ID = "id";
    public static final String ORDER_STATUS = "status";
    public static final String CREATION_TIME = "creation_time";
    public static final String BOOKING_TIME = "booking_time";


    public static final String TABLE_ID = "t_id";
    public static final String TABLE_CAPACITY = "t_capacity";
    public static final String TABLE_STATUS = "t_is_free";

    public static final String CLIENT_ID = "c_id";
    public static final String CLIENT_NAME = "c_name";
    public static final String CLIENT_SURNAME = "c_surname";
    public static final String CLIENT_EMAIL = "c_email";
    public static final String CLIENT_PHONE = "c_phone_number";
    public static final String CLIENT_CARD = "c_card_number";

    public static final String WORKER_ID = "w_id";
    public static final String WORKER_NAME = "w_name";
    public static final String WORKER_SURNAME = "w_surname";
    public static final String WORKER_EMAIL = "w_email";
    public static final String WORKER_PHONE = "w_phone_number";
    public static final String WORKER_ROLE = "w_role";

    public static final String DISH_ID = "id";
    public static final String DISH_NAME = "name";
    public static final String DISH_PRICE = "price";
    public static final String DISH_DESCRIPTION = "description";
}
